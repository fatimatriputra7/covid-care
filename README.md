# Proyek Tengah Semester PBP

Proyek PBP dengan tema COVID-19

**Kelompok E07**

Anggota:
- Evan Christopher (2006597065)
- Fatima Azzahra Triputra (2006597544)
- Lucius Rianto (2006596730)
- Kelvin Erlangga (2006596964)
- Kevin Xavier Emil Aryade Kalsim (2006596756)
- Muhammad Adrian Wirakusumah (2006487351)
- Shabrina Nurmalitasari (2006596705)

**Link Herokuapp:**

https://covid-cares.herokuapp.com/

**Cerita aplikasi yang diajukan serta kebermanfaatannya:**

Informasi mengenai ketersediaan alat kesehatan seputar COVID-19 dan juga forum pertanyaan terkait isu-isu COVID-19 terkini (benchmark dari reddit).

**Daftar modul yang akan diimplementasikan:**
- Registration page
- Login page
- Home page + Headline news seputar COVID-19
- Forum diskusi (Form and Page View)
- COVID-19 Information page
- Vaccine Information Page

**Persona:**

Aplikasi kami diharapkan dapat bermanfaat untuk semua orang yang membutuhkan bantuan seputar COVID-19 dengan cara menampilkan alat kesehatan yang dimiliki agar dapat dipinjam/beli dan juga menggunakan forum diskusi sebagai media bertanya yang dapat dilihat dan dijawab oleh siapapun yang menggunakan aplikasi web ini.
